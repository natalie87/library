#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

conn = sqlite3.connect('library.db')
db = conn.cursor()

db.execute("CREATE TABLE books (ID INT PRIMARY KEY, TITLE TEXT)")
db.execute("CREATE TABLE authors (ID INT PRIMARY KEY, NAME TEXT)")

db.execute("INSERT INTO books VALUES (NULL, 'book 1')")
db.execute("INSERT INTO authors VALUES (NULL, 'author 1')")

db.execute("""CREATE TABLE AuthorsBooks (
                                            ID INT PRIMARY KEY,
                                            authorID INT,
                                            bookID INT,
                                            FOREIGN KEY (authorID) REFERENCES authors(ID),
                                            FOREIGN KEY (bookID) REFERENCES books(ID)
                                        )""")

# SELECT `books`.`title`, GROUP_CONCAT(`authors`.`name`) as `authors` FROM `books`
# INNER JOIN `AuthorBooks` ON `books`.`id`=`AuthorBooks`.`bookID`
# INNER JOIN `authors` ON `AuthorBooks`.`authorID` = `authors`.`id`
# GROUP BY `AuthorsBooks`.`bookID`
