#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask_wtf import Form
from wtforms import TextField
from wtforms.validators import Required

class NewBook(Form):
    title = TextField('title', validators=[Required()])
    authors = TextField('authors', validators=[Required()])

class EditBook(NewBook):
    pass

class EditAuthor(Form):
    name = TextField('name', validators=[Required])