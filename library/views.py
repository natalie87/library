#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import render_template, Flask, request, redirect
from models import Library
from forms import NewBook, EditBook, EditAuthor
from database import db_session

app = Flask(__name__)
app.config.from_object('config')

lib = Library()

@app.route('/', methods=['GET', 'POST'])
def books():
    list_of_books = lib.list_of_books()
    form = NewBook()
    if request.method == 'POST':
        new_book_title = request.form['title']
        new_book_authors = map(lambda x: x.strip(), request.form['authors'].split(','))
        lib.add_book(new_book_title, new_book_authors)
        return redirect('/')
    return render_template('new_book.html',
                           form=form,
                           list_of_books=list_of_books)

@app.template_filter('authors_of_book')
def reverse_filter(book_authors):
    return ', '.join(map(lambda x: '%s' % x, book_authors))

@app.template_filter('books_of_author')
def reverse_filter(author_books):
    return ', '.join(map(lambda x: '%s' % x, author_books))

@app.route('/<id>', methods=['GET', 'POST'])
def edit_book(id):
    book = lib.get_book(id)
    form = EditBook(obj=book)
    def authors_string_to_list(authors_list):
        ls = map(lambda x: x.strip(), authors_list.replace('[', '').replace(']', '').split(','))
        print ls
        return ls
    if request.method == 'POST':
        edit_book_title = request.form['title']
        edit_book_authors = authors_string_to_list(request.form['authors'])
        lib.edit_book(id, edit_book_title, edit_book_authors)
        return redirect('/')
    return render_template('edit_book.html',
                           form=form)

@app.route('/authors', methods=["GET", "POST"])
def authors():
    list_of_authors = lib.list_of_authors()
    return render_template('authors.html',
                           list_of_authors=list_of_authors)

@app.route('/authors/del/<id>', methods=["GET"])
def del_author(id):
    lib.del_author(id)
    return redirect('/authors')

@app.route('/del/<id>', methods=["GET"])
def del_book(id):
    lib.del_book(id)
    return redirect('/')

@app.route('/authors/edit/<id>', methods=['GET', "POST"])
def edit_author_name(id):
    edit_author = lib.get_author(id)
    form = EditAuthor(obj=edit_author)
    if request.method == 'POST':
        new_name_author = request.form['name']
        lib.edit_author(id, new_name_author)
        return redirect('/authors')
    return render_template('edit_author.html',
                           form=form)

@app.teardown_request
def shutdown_session(exception=None):
    db_session.remove()

if __name__ == "__main__":
    app.run(debug=True)