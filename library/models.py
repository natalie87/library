#!/usr/bin/env python
# -*- coding: utf-8 -*-

from database import Base, db_session, init_db
from sqlalchemy import Column, Integer, String, Table, ForeignKey, func
from sqlalchemy.orm import relationship

association = Table('association', Base.metadata,
                    Column('author_id', Integer, ForeignKey('authors.id')),
                    Column('book_id', Integer, ForeignKey('books.id')),
                    extend_existing=True
)

class Book(Base):
    __tablename__ = 'books'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    title = Column(String, unique=True)
    authors = relationship('Author', uselist=True, secondary=association, backref='books')

    def __init__(self, title, authors):
        self.title = title
        self.authors = authors

    def __repr__(self):
        return "%s" % (self.title)

class Author(Base):
    __tablename__ = 'authors'
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "%s" % (self.name)

class Library:

    def __init__(self):
        init_db()
        self.session = db_session()

    def add_book(self, book_title, book_authors):
        if book_title and book_authors:
            list_of_authors = []

            for author_name in book_authors:
                author = self.session.query(Author).filter_by(name=author_name).first()
                if not author:
                    author = Author(author_name)
                    self.session.add(author)
                list_of_authors.append(author)

            new_book = self.session.query(Book).filter_by(title=book_title).first()
            if not new_book:
                new_book = Book(book_title, list_of_authors)
                self.session.add(new_book)
            else:
                for a in list_of_authors:
                    new_book.authors.append(a)
            self.session.commit()
    def get_book(self, book_id):
        return self.session.query(Book).filter(Book.id == book_id).first()

    def get_author(self, author_id):
        return self.session.query(Author).filter(Author.id == author_id).first()

    def edit_book(self, book_id, book_title=None, book_authors=None):
        if book_title:
            edit_title = self.session.query(Book).filter_by(title=book_title).first()
            if not edit_title:
                self.get_book(book_id).title = book_title
        if book_authors:
            list_of_authors = []
            for author_name in book_authors:
                author = self.session.query(Author).filter_by(name=author_name).first()
                if not author:
                    author = Author(author_name)
                    self.session.add(author)
                list_of_authors.append(author)
            self.get_book(book_id).authors = list_of_authors
        self.session.commit()

    def list_of_books(self):
        return self.session.query(Book).all()

    def list_of_authors(self):
        return self.session.query(Author).all()

    def del_book(self, book_id):
        self.session.query(Book).filter(Book.id == book_id).delete()
        self.session.commit()

    def del_author(self, author_id):
        self.session.query(Author).filter(Author.id == author_id).delete()
        self.session.commit()

    def edit_author(self, author_id, author_name):
        edit_author = self.get_author(author_id)
        edit_author.name = author_name
        self.session.commit()
