#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

CSRF_ENABLED = True
SECRET_KEY = 'key'

PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
DATABASE_URI = 'sqlite:///' + os.path.join(PROJECT_PATH, 'library.db')